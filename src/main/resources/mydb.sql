-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 20 Janvier 2017 à 10:09
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `mydb`
--

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `event`
--

INSERT INTO `event` (`id`, `title`, `description`) VALUES
(12, 'test', 'test'),
(13, 'finaltest', 'desc'),
(14, 'youpi', 'youpi'),
(15, 'rt', 'rt'),
(16, 'tr', 'tr'),
(17, 'oi', 'oi'),
(18, 'bn', 'bn'),
(19, 'qs', 'qs'),
(20, 'ev1', 'ev1'),
(21, 'de', 'de'),
(22, 'et', 'et'),
(23, 'gvrfgvfr', 'gergerg'),
(24, 'hujg', 'fgf'),
(25, 'gfged', 'gergerg'),
(26, 'az', 'az');

-- --------------------------------------------------------

--
-- Structure de la table `invitation`
--

CREATE TABLE IF NOT EXISTS `invitation` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `date` varchar(45) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_has_user_user2_idx` (`user_id`),
  KEY `fk_event_has_user_event1_idx` (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `invitation`
--

INSERT INTO `invitation` (`event_id`, `user_id`, `id`, `email`, `date`, `token`, `status`) VALUES
(14, 9, 8, 'oui', 'Thu Jan 19 11:38:19 CET 2017', '589b49c221b22698a7ace78f481c110039fab7b0', 'pending'),
(14, 10, 9, 'ok', 'Thu Jan 19 11:40:01 CET 2017', 'a397993919691c8176dbf660a3d00f3586274365', 'pending'),
(15, 11, 10, 'az', 'Thu Jan 19 11:40:24 CET 2017', 'f5f875b4b5e55da01b6f25db427e4859bf66bf82', 'pending'),
(15, 12, 11, 't', 'Thu Jan 19 11:43:01 CET 2017', '00894533b744f4bf0394006991b503f342cb27eb', 'pending'),
(16, 13, 12, 'zr', 'Thu Jan 19 11:45:47 CET 2017', '37d7f2452c1bf4a459810dd3982c4462f3cfa611', 'pending'),
(17, 14, 13, 'p', 'Thu Jan 19 11:48:02 CET 2017', '5b769fefc0c3340f210cf48eb09400ea92add2e0', 'pending'),
(18, 15, 14, 'wx', 'Thu Jan 19 11:50:33 CET 2017', '16482da83f1dfc723905aa2d065b61d9e1a71768', 'declined'),
(19, 16, 15, 'df', 'Thu Jan 19 11:54:27 CET 2017', '170193006548ca5e0f2a24b7d27a3132cc6839a1', 'declined'),
(20, 17, 16, 'po', 'Thu Jan 19 12:07:25 CET 2017', '8aed0d669b26b8dde28734e5cb6fac9a5697fb5d', 'confirmed'),
(21, 18, 17, 'az', 'Thu Jan 19 13:41:52 CET 2017', '8044e75d0e167069cf6784dc97f855000fbe8649', 'confirmed'),
(22, 19, 18, 'zd', 'Thu Jan 19 13:47:21 CET 2017', '262f5a3dc097b60baf8940b6d17896cdb9cd39a2', 'confirmed'),
(23, 20, 19, 'grgrgr', 'Thu Jan 19 13:50:25 CET 2017', '5d9a371ee40197c7b2ae1abea062df298d2292ff', 'confirmed'),
(24, 21, 20, 'dtdy', 'Thu Jan 19 16:52:39 CET 2017', 'bec78c2d90b09f73f3490c956f1a2213aec3ec50', 'pending'),
(25, 22, 21, 'gegerg', 'Thu Jan 19 16:54:13 CET 2017', 'e337439515f5b5dbcf17426d6914e35cd3493fae', 'confirmed'),
(12, 23, 22, 'gergrezg', 'Fri Jan 20 09:19:45 CET 2017', '874f39677927d1263793a868814cfa6edae0a11e', 'declined'),
(12, 24, 23, 'hrthrt', 'Fri Jan 20 09:20:56 CET 2017', '13b1ba42c123c12169c21896a02b3de4d4bda9ad', 'confirmed'),
(12, 25, 24, 'abc', 'Fri Jan 20 09:24:03 CET 2017', '445f84402441fdaabe4957593abe1e98cd85acb3', 'declined'),
(26, 26, 25, 'tr', 'Fri Jan 20 09:56:10 CET 2017', '8045fe27890316a8177e04f629499c6cc4a3c498', 'declined');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `email`) VALUES
(4, NULL, NULL, 'abc'),
(5, NULL, NULL, 'abc'),
(6, NULL, NULL, 'abc'),
(7, NULL, NULL, 'abc'),
(8, NULL, NULL, 'aze'),
(9, NULL, NULL, 'oui'),
(10, NULL, NULL, 'ok'),
(11, NULL, NULL, 'az'),
(12, NULL, NULL, 't'),
(13, NULL, NULL, 'zr'),
(14, NULL, NULL, 'p'),
(15, NULL, NULL, 'wx'),
(16, NULL, NULL, 'df'),
(17, NULL, NULL, 'po'),
(18, NULL, NULL, 'az'),
(19, NULL, NULL, 'zd'),
(20, NULL, NULL, 'grgrgr'),
(21, NULL, NULL, 'dtdy'),
(22, NULL, NULL, 'gegerg'),
(23, NULL, NULL, 'gergrezg'),
(24, NULL, NULL, 'hrthrt'),
(25, NULL, NULL, 'abc'),
(26, NULL, NULL, 'tr');

-- --------------------------------------------------------

--
-- Structure de la table `user_participate`
--

CREATE TABLE IF NOT EXISTS `user_participate` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_event_has_user_user1_idx` (`user_id`),
  KEY `fk_event_has_user_event_idx` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `invitation`
--
ALTER TABLE `invitation`
  ADD CONSTRAINT `fk_event_has_user_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_event_has_user_user2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user_participate`
--
ALTER TABLE `user_participate`
  ADD CONSTRAINT `fk_event_has_user_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_event_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
