<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Event creation</title>
</head>
<body>
	<jsp:include page="banner.jsp" />
	<jsp:include page="flashScope.jsp" />
	<form action="EventCreationServlet" method="POST"
		enctype="multipart/form-data">
		<label for="title">Title</label> <input id="title" type="text"
			name="title" />
		<c:if test="${ (not empty errors) and (not empty errors['title']) }">
			<span> ${ errors['title'] }</span>
		</c:if>
		<label for="description">Description</label> <input id="description"
			type="text" name="description" />
		<c:if
			test="${ (not empty errors) and (not empty errors['description']) }">
			<span> ${ errors['description'] }</span>
		</c:if>
		<input type="file" name="file" /> <input type="submit"
			value="Register" />
	</form>

</body>
</html>