<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmation</title>
</head>
<body>
	<h1>Confirmation presence</h1>

	<form action="AttendanceConfirmationServlet" method="POST">
		<button type="submit" name="confirm" value=True>Confirm</button>
		<button type="submit" name="confirm" value=False>Decline</button>
		<input type="hidden" name="index" value=${ index } /> <input
			type="hidden" name="token" value=${ token } />
	</form>

</body>
</html>