<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Event creation</title>
</head>
<body>

	<form action="RegisterServlet" method="POST">
		<label for="username">Username</label> <input id="username"
			type="text" name="username" /> <label for="password">Password</label>
		<input id="password" type="text" name="password" />
		<c:if
			test="${ (not empty errors) and (not empty errors['password']) }">
			<span> ${ errors['password'] }</span>
		</c:if>
		<c:if
			test="${ (not empty errors) and (not empty errors['usernameExists']) }">
			<span> ${ errors['usernameAlreadyExists'] }</span>
		</c:if>
		<input type="submit" value="Register" />
	</form>

</body>
</html>