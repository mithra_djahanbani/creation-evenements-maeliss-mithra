<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List events</title>
</head>
<body>
	<jsp:include page="banner.jsp" />
	<jsp:include page="flashScope.jsp" />

	<p>Liste des evenements</p>
	<c:forEach var="item" items="${ events }" varStatus="status">
		<p>
			<a
				href="http://localhost:8080/creation-evenements-maeliss-mithra/EventLinkServlet?index=${ item.id }">
				${ item.title }</a>
		</p>
	</c:forEach>
	<form action="ListEventServlet" method="POST">

		<button>Create new event</button>

	</form>
</body>
</html>