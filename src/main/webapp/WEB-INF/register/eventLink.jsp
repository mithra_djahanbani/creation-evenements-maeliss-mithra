<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Event Link</title>
<link rel="stylesheet" href="eventLink.css" />
</head>
<body>
	<jsp:include page="banner.jsp" />
	<jsp:include page="flashScope.jsp" />

	<nav>
		<ul>
			<li><a
				href="http://localhost:8080/creation-evenements-maeliss-mithra/EventDescriptionServlet?index=${ event.id }">Event</a></li>
			<li><a
				href="http://localhost:8080/creation-evenements-maeliss-mithra/EventLinkServlet?index=${ event.id }">Status</a></li>
			<li><a
				href="http://localhost:8080/creation-evenements-maeliss-mithra/EventStatsServlet?index=${ event.id }">Statistics</a></li>
		</ul>
	</nav>
	<h1>Evenement</h1>
	<p>Auteur : ${ event.author }</p>
	<p>Titre : ${ event.title }</p>
	<p>Description : ${ event.description }</p>

	<p>
		<img
			src="http://localhost:8080/creation-evenements-maeliss-mithra/ImageServlet?index=${ event.id }"
			alt="Poster" />
	</p>

	<p>Invitations @</p>
	<form action="EventLinkServlet" method="POST">
		<textarea name="textarea" rows="10" cols="50"></textarea>
		<c:if
			test="${ (not empty errors) and (not empty errors['adresses']) }">
			<span> ${ errors['adresses'] }</span>
		</c:if>
		<input type="submit" value="Register" /> <input type="hidden"
			name="index" value="${ event.id }" />
	</form>

	<div class="tab">
		<div class="tab__item">
			<table>
				<tr>
					<th>Invitations pending</th>
				</tr>
				<tr>
					<th>email</th>
					<th>date</th>
				</tr>
				<c:forEach var="item" items="${ pendingInvitations }"
					varStatus="status">
					<tr>
						<td>${ item.email }</td>
						<td>${ item.date }</td>
					</tr>
				</c:forEach>
			</table>
		</div>

		<div class="tab__item">
			<table>
				<tr>
					<th>Invitations confirmed</th>
				</tr>
				<tr>
					<th>email</th>
					<th>date</th>
				</tr>
				<c:forEach var="item" items="${ confirmedInvitations }"
					varStatus="status">
					<tr>
						<td>${ item.email }</td>
						<td>${ item.date }</td>
					</tr>
				</c:forEach>
			</table>
		</div>

		<div class="tab__item">
			<table>
				<tr>
					<th>Invitations declined</th>
				</tr>
				<tr>
					<th>email</th>
					<th>date</th>
				</tr>
				<c:forEach var="item" items="${ declinedInvitations }"
					varStatus="status">
					<tr>
						<td>${ item.email }</td>
						<td>${ item.date }</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>