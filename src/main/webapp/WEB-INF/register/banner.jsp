<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Event creation</title>
</head>
<body>
	<form action="Banner" method="POST">
		<c:if test="${ not isConnected }">
			<button type="submit" name="log" value=True>Login</button>
		</c:if>
		<c:if test="${ isConnected }">
			<button type="submit" name="log" value=False>Logout</button>
		</c:if>
		<button type="submit" name="log" value="Register">Register</button>
	</form>

</body>
</html>