package com.aiconoa.trainings.cdi;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.aiconoa.trainings.entity.Participation;
import com.aiconoa.trainings.service.EventServiceException;

@ApplicationScoped
public class MailObserver {

	@Resource(name = "java:jboss/mail/Gmail")
	private Session session;

	public void sendMail(@Observes Participation participation){
		
		String hash = participation.getHash();
		
		try {
			Message message = new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("mithramaeliss@gmail.com"));
			message.setSubject("Invitation evenement");
			message.setText(
					"Veuillez confirmer votre présence\n http://localhost:8080/creation-evenements-maeliss-mithra/faces/attendance-confirmation.xhtml?token="
							+ hash);
			Transport.send(message);
		} catch (MessagingException e) {
			throw new EventServiceException("Can't send email", e);
		}
	}
}
