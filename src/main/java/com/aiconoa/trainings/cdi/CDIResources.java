package com.aiconoa.trainings.cdi;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;

import com.aiconoa.trainings.service.EventDatabase;

@ApplicationScoped
public class CDIResources {
	
	@Produces @EventDatabase @Resource(lookup = "java:jboss/DataSources/EventDS")
	private DataSource eventDS;
}
