package com.aiconoa.trainings.jsf;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.ejb.RandomNumberEJB;

@Named
@ViewScoped
public class GuessANumberBean implements Serializable{

	private static final Logger LOGGER = Logger.getLogger(GuessANumberBean.class.getName());
	
	private Integer numberToGuess;
	
	/*@NotEmpty
	@Min(1)
	@Max(10)*/
//	@Range(min=1, max=10)
	private Integer submittedNumber;
	
	private boolean result;
	
	@Inject
	private RandomNumberEJB randomNumberEJB;
	
	/*public GuessANumberBean() {
		numberToGuess = randomNumberEJB.generateRandomInteger(10);
	}*/
	
	@PostConstruct//pour faire les injections avant d'appeler le constructeur
	public void init(){
		numberToGuess = randomNumberEJB.generateRandomInteger(10);
	}

	public Integer getSubmittedNumber(){
		LOGGER.info(String.format("getSubmittedNumber - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		return submittedNumber;
	}

	public void setSubmittedNumber(Integer submittedNumber) {
		LOGGER.info(String.format("setSubmittedNumber - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		this.submittedNumber = submittedNumber;
	}
	
	public void guess(){
		LOGGER.info("number to guess" + numberToGuess);
		LOGGER.info(String.format("guess - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		result = (submittedNumber.equals(numberToGuess));
	}

	public boolean getResult() {
		LOGGER.info(String.format("getResult - %s", FacesContext.getCurrentInstance().getCurrentPhaseId()));
		return result;
	}
	
}
