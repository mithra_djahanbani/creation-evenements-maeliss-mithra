package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.event.Event;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.entity.Invitation;
import com.aiconoa.trainings.entity.Participation;
import com.aiconoa.trainings.service.EventService;

@Named
@ViewScoped
public class EventLinkBean implements Serializable{

	private static final Logger LOGGER = Logger.getLogger(EventLinkBean.class.getName());

	@Inject
	private EventService eventService;

	@Inject
	Event<Participation> myEvent;

	private Integer index;
	private String addressesParam;
	private List<List<Invitation>> invitations;

	public List<Invitation> getPendingInvitations() {
		return invitations.get(0);
	}

	public List<Invitation> getConfirmedInvitations() {
		return invitations.get(1);
	}

	public List<Invitation> getDeclinedInvitations() {
		return invitations.get(2);
	}

	public List<List<Invitation>> getInvitations() {
		return invitations;
	}

	public void load() {
		this.invitations = eventService.updateInvitationsStateOfEvent(index);
	}

	public String getAddressesParam() {
		return addressesParam;
	}

	public void setAddressesParam(String addressesParam) {
		this.addressesParam = addressesParam;
	}

	public Integer getIndex() {
		LOGGER.info("get query string index");
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Evenement getEvent() {
		return eventService.selectEvent(index);
	}

	public void register() throws IOException{
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		
		if (session.getAttribute("username") != null) {
			String[] adresses = addressesParam.split(";");
			for (String email : adresses) {
				if (!eventService.isEmailAlreadySet(email)) {
					int userId = eventService.insertUser(email).getId();
					String hash = eventService.generateHash(email, index);
					myEvent.fire(new Participation(hash));
					eventService.insertInvitation(email, index, userId, hash);
					load();
				}
			}
		} else {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
	}
}
