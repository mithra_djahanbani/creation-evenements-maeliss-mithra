package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.service.EventService;

@Named
@RequestScoped
public class ListEvent {

	private static final Logger LOGGER = Logger.getLogger(ListEvent.class.getName());

	@Inject
	private EventService eventService;
	


	public List<Evenement> getEvents(){
		LOGGER.info("called selectEvents()");
		return eventService.selectEvents();
	}
	
	public void create() throws IOException{
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		LOGGER.info("check session before creating an event");
		if (session.getAttribute("username") != null) {
			FacesContext.getCurrentInstance().getExternalContext().redirect("event-creation-form.xhtml");
		} else {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
	}

	public int getAuthorId() {
		int authorId = 0;
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		if (session.getAttribute("username") != null) {
			String authorIndex = session.getAttribute("id").toString();
			authorId = Integer.parseInt(authorIndex);
		}
		return authorId;
	}
}
