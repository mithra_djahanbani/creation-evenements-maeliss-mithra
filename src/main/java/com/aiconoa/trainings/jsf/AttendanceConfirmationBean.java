package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.service.EventService;

@Named
@RequestScoped
public class AttendanceConfirmationBean {

	private static final Logger LOGGER = Logger.getLogger(AttendanceConfirmationBean.class.getName());

	@Inject
	private EventService eventService;
	
	
	private String token;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void confirm() throws IOException{
		eventService.updateStatus("confirmed", token);
		String eventId = String.valueOf(eventService.findEventIdByToken(token));
		FacesContext.getCurrentInstance().getExternalContext().redirect("event-link.xhtml?index="+eventId);
	}
	
	public void decline() throws IOException{
		eventService.updateStatus("declined", token);
		String eventId = String.valueOf(eventService.findEventIdByToken(token));
		FacesContext.getCurrentInstance().getExternalContext().redirect("event-link.xhtml?index="+eventId);
	}
}
