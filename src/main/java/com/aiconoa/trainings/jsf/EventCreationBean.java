package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.service.EventService;

@Named
@ViewScoped
public class EventCreationBean implements Serializable{

	private static final Logger LOGGER = Logger.getLogger(EventCreationBean.class.getName());

	@Inject
	private EventService eventService;

	private String title;
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void register() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		if (session.getAttribute("username") != null) {
			String authorIndex = session.getAttribute("id").toString();
			int authorId= Integer.parseInt(authorIndex);
			eventService.insertEvent(title, description, "", authorId);
			FacesContext.getCurrentInstance().getExternalContext().redirect("list-event.xhtml");
			} else {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
		
		

	}

}
