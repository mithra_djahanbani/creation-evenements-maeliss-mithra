package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.service.EventService;

@Named
@ViewScoped
public class RegisterBean implements Serializable{
	
	@Inject
	private EventService eventService;
	
	private boolean isUsernameAlreadySet=false;
	
	private Author author = new Author();
	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	
	public boolean getIsUsernameAlreadySet() {
		return isUsernameAlreadySet;
	}

	public void setUsernameAlreadySet(boolean isUsernameAlreadySet) {
		this.isUsernameAlreadySet = isUsernameAlreadySet;
	}

	public void register() throws IOException{
		if (eventService.isUsernameAlreadySet(author.getUsername())) {
			isUsernameAlreadySet=true;
			FacesContext.getCurrentInstance().getExternalContext().redirect("register.xhtml");
		}
		eventService.insertAuthor(author.getUsername(), author.getPassword());
		FacesContext.getCurrentInstance().getExternalContext().redirect("list-event.xhtml");
	}

}
