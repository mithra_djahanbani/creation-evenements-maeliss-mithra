package com.aiconoa.trainings.jsf;

import java.util.Random;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class HelloJSF {

	private Integer random;
	
	public String getHello(){
		return "hello jsf :-)";
	}
	
	public Integer getRandom(){
		if (random == null) {
			random = new Random().nextInt();
		}
		return random;
	}
}
