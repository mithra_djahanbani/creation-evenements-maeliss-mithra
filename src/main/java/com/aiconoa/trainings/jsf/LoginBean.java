package com.aiconoa.trainings.jsf;

import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.service.EventService;

@Named
@ApplicationScoped
public class LoginBean {
	
	@Inject
	private EventService eventService;

	private Author author = new Author();
	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public void register() throws IOException {
		if (eventService.isAuthorValid(author.getUsername(), author.getPassword())) {
			int authorId = eventService.getAuthorId(author.getUsername());
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletRequest request = (HttpServletRequest) ec.getRequest();
			HttpSession session = request.getSession();
			session.setAttribute("username", author.getUsername());
			session.setAttribute("id", authorId);
		} else {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
		
		
		FacesContext.getCurrentInstance().getExternalContext().redirect("list-event.xhtml");
	}

}
