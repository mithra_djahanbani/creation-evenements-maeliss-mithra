package com.aiconoa.trainings.jsf;

import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class QueryBean {
	
	private static final Logger LOGGER = Logger.getLogger(QueryBean.class.getName());

	
	private Integer id;

	public Integer getId() {
		LOGGER.info("get index");
		return id;
	}

	public void setId(Integer id) {
		LOGGER.info("set index");
		this.id = id;
	}
	
	public String load(){
		return "hello-jsf";
	}
	
}
