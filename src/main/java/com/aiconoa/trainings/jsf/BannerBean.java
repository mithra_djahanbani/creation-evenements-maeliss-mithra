package com.aiconoa.trainings.jsf;

import java.io.IOException;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Named
@RequestScoped
public class BannerBean {

	private static final Logger LOGGER = Logger.getLogger(BannerBean.class.getName());
	
	public boolean getIsConnected() {
		boolean isConnected = false;
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		if (session.getAttribute("username") != null) {
			LOGGER.info("check session");
			isConnected = true;
		}
		return isConnected;
	}

	public void invalidateSession() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		request.getSession().invalidate();
		FacesContext.getCurrentInstance().getExternalContext().redirect("list-event.xhtml");
	}

	public void goToLoginPage() throws IOException {
		LOGGER.info("redirection vers login page");
		FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
	}

	public void register() throws IOException {
		LOGGER.info("redirection sur register");
		FacesContext.getCurrentInstance().getExternalContext().redirect("register.xhtml");
	}
	
	public String getSessionUsername() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		HttpSession session = request.getSession();
		return session.getAttribute("username").toString();
	}

}
