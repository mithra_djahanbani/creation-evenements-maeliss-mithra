package com.aiconoa.trainings.jsf;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

@Named
@RequestScoped
public class EmailValidator implements Validator {
	
	
	@Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		boolean isEmailValid = false;
		String addressesParam = String.valueOf(value);
        String[] adresses = addressesParam.split(";");
		for (String email : adresses) {
			if (email.matches("^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$")) {
				isEmailValid = true;
			}
		}

        if (!isEmailValid) {
            throw new ValidatorException(new FacesMessage("Email is invalid!"));
        }
    }


}
