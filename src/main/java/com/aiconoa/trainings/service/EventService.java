package com.aiconoa.trainings.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.entity.Invitation;
import com.aiconoa.trainings.entity.User;

@Stateless
public class EventService {

	@PersistenceUnit(unitName = "creation-evenements-maeliss-mithra")
	private EntityManagerFactory emf;

	@Inject
	@EventDatabase
	private DataSource eventDS;

	public Invitation findInvitationByUserAndEvent(int userId, int eventId){
		try {
			EntityManager em = emf.createEntityManager();
			User user = selectUser(userId);
			Evenement event = selectEvent(eventId);
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.user = :user AND invitation.event = :event";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("user", user);
			query.setParameter("event", event);
			return query.getSingleResult();
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectInvitationsByUserIdAndEvent", e);
		}
	}
	
	public List<Invitation> findInvitationsByUser(int userId){
		try {
			EntityManager em = emf.createEntityManager();
			User user = selectUser(userId);
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.user = :user";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("user", user);
			return query.getResultList();
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectInvitationsByUserId", e);
		}
	}
	
	public int findEventIdByToken(String token) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.token = :token";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("token", token);
			Invitation invitation = query.getSingleResult();
			return invitation.getEvent().getId();
		} catch (NoResultException e) {
			throw new EventServiceException("Error find eventId by token", e);
		}
	}

	public void updateStatus(String confirmed, String token) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "UPDATE Invitation invitation SET invitation.status = :status WHERE invitation.token = :token";
			Query query = em.createQuery(jpql);
			query.setParameter("status", confirmed);
			query.setParameter("token", token);
			query.executeUpdate();
		} catch (NoResultException e) {
			throw new EventServiceException("Error updateStatus", e);
		}
	}

	public Evenement insertEvent(String title, String description, String fileName, int authorId) {
		try {
			EntityManager em = emf.createEntityManager();
			Author author = selectAuthor(authorId);
			Evenement event = new Evenement();
			event.setTitle(title);
			event.setDescription(description);
			event.setImage(fileName);
			event.setAuthor(author);
			em.persist(event);
			return event;
		} catch (PersistenceException e) {
			throw new EventServiceException("Error insertEvent", e);
		}
	}

	private Author selectAuthor(int authorId) {
		try {
			EntityManager em = emf.createEntityManager();
			return em.find(Author.class, authorId);
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectAuthor", e);
		}
	}

	public Evenement selectEvent(int eventId) {
		try {
			EntityManager em = emf.createEntityManager();
			return em.find(Evenement.class, eventId);
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectEvent", e);
		}
	}

	public List<Invitation> selectInvitationsByEventId(int eventId) {
		try {
			EntityManager em = emf.createEntityManager();
			Evenement event = selectEvent(eventId);
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.event = :event";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("event", event);
			return query.getResultList();
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectInvitationsByEventId", e);
		}
	}

	public User insertUser(String email) {
		try {
			EntityManager em = emf.createEntityManager();
			User user = new User();
			user.setEmail(email);
			em.persist(user);
			return user;
		} catch (PersistenceException e) {
			throw new EventServiceException("Error insertUser", e);
		}
	}

	public String generateHash(String email, int eventId) {
		try {
			String concat = email + "#" + eventId;
			String hash;
			hash = toSHA1(concat.getBytes("UTF-8"));
			return hash;
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			throw new EventServiceException("Can't generate hash", e);
		}
	}

	public String generateUsernameToken(String username) {
		try {
			String hash;
			hash = toSHA1(username.getBytes("UTF-8"));
			return hash;
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			throw new EventServiceException("Can't generate hash", e);
		}
	}
	
	private String toSHA1(byte[] bytes) throws NoSuchAlgorithmException {
		return byteArrayToHexString(MessageDigest.getInstance("SHA1").digest(bytes));
	}

	private String byteArrayToHexString(byte[] b) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}

	public Invitation insertInvitation(String email, int eventId, int userId, String hash) {
		Calendar calendar = Calendar.getInstance();
		String time = calendar.getTime().toString();
		try {
			EntityManager em = emf.createEntityManager();
			Evenement event = selectEvent(eventId);
			User user = selectUser(userId);
			Invitation invitation = new Invitation();
			invitation.setDate(time);
			invitation.setEmail(email);
			invitation.setToken(hash);
			invitation.setEvent(event);
			invitation.setUser(user);
			invitation.setStatus("pending");
			em.persist(invitation);
			return invitation;
		} catch (PersistenceException e) {
			throw new EventServiceException("Error insertInvitation", e);
		}
	}

	public User selectUser(int userId) {
		try {
			EntityManager em = emf.createEntityManager();
			return em.find(User.class, userId);
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectUser", e);
		}
	}

	public List<Evenement> selectEvents() {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT event FROM Evenement event";
			TypedQuery<Evenement> query = em.createQuery(jpql, Evenement.class);
			return query.getResultList();
		} catch (NoResultException e) {
			throw new EventServiceException("Error selectEvents", e);
		}
	}

	public boolean isTokenValid(String token) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.token = :token";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("token", token);
			return !query.getResultList().isEmpty();
		} catch (NoResultException e) {
			throw new EventServiceException("Error isTokenValid", e);
		}
	}

	public boolean isEmailAlreadySet(String email) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT invitation FROM Invitation invitation WHERE invitation.email = :email";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("email", email);
			return !query.getResultList().isEmpty();
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error isEmailAlreadySet", e);
		}
	}

	public String selectNameOfImageFromEventId(String eventId) {
		try {
			EntityManager em = emf.createEntityManager();
			Evenement event = em.find(Evenement.class, eventId);
			return event.getImage();
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error selectNameOfImageFromEventId", e);
		}
	}

	public boolean isAuthorValid(String username, String password) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT author FROM Author author WHERE author.username = :username AND author.password = :password";
			TypedQuery<Author> query = em.createQuery(jpql, Author.class);
			query.setParameter("username", username);
			query.setParameter("password", password);
			return !query.getResultList().isEmpty();
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error isAuthorValid", e);
		}
	}

	public boolean isEventFromAuthor(int eventId, int authorId) {
		try {
			EntityManager em = emf.createEntityManager();
			Evenement event = em.find(Evenement.class, eventId);
			return event.getAuthor().getId() == authorId;
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error isEventFromAuthor", e);
		}
	}

	public Author insertAuthor(String username, String password) {
		try {
			EntityManager em = emf.createEntityManager();
			Author author = new Author();
			author.setUsername(username);
			author.setPassword(password);
			em.persist(author);
			return author;
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error insertAuthor", e);
		}
	}

	
	public int getAuthorId(String username) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT author FROM Author author WHERE author.username = :username";
			TypedQuery<Author> query = em.createQuery(jpql, Author.class);
			query.setParameter("username", username);
			return query.getSingleResult().getId();
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error getAuthorId", e);
		}
	}

	public boolean isUsernameAlreadySet(String username) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT author FROM Author author WHERE author.username = :username";
			TypedQuery<Author> query = em.createQuery(jpql, Author.class);
			query.setParameter("username", username);
			return !query.getResultList().isEmpty();
		} catch (IllegalStateException e) {
			throw new EventServiceException("Error isUsernameAlreadySet", e);
		}
	}

	public List<List<Invitation>> updateInvitationsStateOfEvent(int eventId) {
		List<List<Invitation>> listInvitations = new ArrayList<List<Invitation>>();
		
		List<Invitation> invitations = selectInvitationsByEventId(eventId);
		List<Invitation> pendingInvitations = new ArrayList<>();
		List<Invitation> confirmedInvitations = new ArrayList<>();
		List<Invitation> declinedInvitations = new ArrayList<>();
		for (Invitation invitation : invitations) {
			if ((invitation != null) && (invitation.getStatus() != null)) {
				if (invitation.getStatus().equals("pending")) {
					pendingInvitations.add(invitation);
				}
				if (invitation.getStatus().equals("confirmed")) {
					confirmedInvitations.add(invitation);
				}
				if (invitation.getStatus().equals("declined")) {
					declinedInvitations.add(invitation);
				}
			}
		}
		listInvitations.add(pendingInvitations);
		listInvitations.add(confirmedInvitations);
		listInvitations.add(declinedInvitations);
		return listInvitations;
	}

	public void provideAuthorSessionId(String username, String token) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "UPDATE Author author SET author.sessionId = :sessionId WHERE author.username = :username";
			Query query = em.createQuery(jpql);
			query.setParameter("sessionId", token);
			query.setParameter("username", username);
			query.executeUpdate();
		} catch (NoResultException e) {
			throw new EventServiceException("Error updateStatus", e);
		}
		
	}

	public boolean validateToken(String token) {
		try {
			EntityManager em = emf.createEntityManager();
			String jpql = "SELECT author FROM Author author WHERE author.sessionId = :sessionId";
			TypedQuery<Invitation> query = em.createQuery(jpql, Invitation.class);
			query.setParameter("sessionId", token);
			return !query.getResultList().isEmpty();
		} catch (NoResultException e) {
			throw new EventServiceException("Error isTokenValid", e);
		}		
	}
}
