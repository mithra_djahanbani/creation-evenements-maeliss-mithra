package com.aiconoa.trainings.service;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpSession;

@ApplicationScoped
public class Validation {

	public boolean isParameterNumerical(String ind) {
		return ind.matches("^[0-9]+$");
	}

	public Map<String, String> isEmailValid(String adressesParam) {
		Map<String, String> errors = new HashMap<>();
		if (adressesParam == null || adressesParam.trim().isEmpty()) {
			errors.put("adresses", "An adress must be specified");
		}
		return errors;
	}

	public boolean hasSessionUsername(HttpSession session) {
		return session.getAttribute("username") != null;
	}
	
}
