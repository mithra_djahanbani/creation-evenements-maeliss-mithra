package com.aiconoa.trainings.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/ImageServlet")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(ImageServlet.class.getName());

	@Inject
	private EventService eventService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String eventId = request.getParameter("index");
		try {
			String image = eventService.selectNameOfImageFromEventId(eventId);
			File file = new File("C:/formationJava/tmp", image);
			Files.copy(file.toPath(), response.getOutputStream());
			response.getOutputStream();
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error with EventService", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}
}
