package com.aiconoa.trainings.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Banner
 */
@WebServlet("/Banner")
public class Banner extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String submit = request.getParameter("log");
		if (submit.matches("False")) {
			request.getSession().invalidate();
			
			HttpSession session = request.getSession();
			request.setAttribute("flash.message", "Déconnexion reussie !");
			response.sendRedirect("ListEventServlet");
		}
		if (submit.matches("True")) {
			response.sendRedirect("Login");
		}
		if (submit.matches("Register")) {
			response.sendRedirect("RegisterServlet");
		}
		
	}

}
