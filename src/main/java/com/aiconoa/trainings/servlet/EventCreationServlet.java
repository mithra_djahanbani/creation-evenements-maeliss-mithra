package com.aiconoa.trainings.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;
import com.aiconoa.trainings.service.Validation;

/**
 * Servlet implementation class EventCreationServlet
 */
@WebServlet("/EventCreationServlet")
@MultipartConfig
public class EventCreationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(EventCreationServlet.class.getName());

	@Inject
	private EventService eventService;
	
	@Inject
	private Validation validation;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		if (!isSession) {
			response.sendRedirect("Login");
		}
		request.setAttribute("isConnected", isSession);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/eventCreationForm.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int authorId = 0;
		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		if (!isSession) {
			response.sendError(404);
		} else {
			String authorIndex = session.getAttribute("id").toString();
			if (!authorIndex.matches("^[0-9]+$")) {
				response.sendError(404);
				return;
			}
			authorId = Integer.parseInt(authorIndex);
		}

		Part filePart = request.getPart("file");
		String fileName = filePart.getSubmittedFileName();
		if (!fileName.equals("")) {
			InputStream is = filePart.getInputStream();
			File outputFile = new File("C:/formationJava/tmp", fileName);
			Files.copy(is, outputFile.toPath());
		}

		String title = request.getParameter("title");
		String description = request.getParameter("description");
		Map<String, String> errors = new HashMap<>();

		if (title == null || title.trim().isEmpty()) {
			errors.put("title", "A title must be specified");
		}
		if (description == null || description.trim().isEmpty()) {
			errors.put("description", "A description must be specified");
		}
		if (!errors.isEmpty()) {
			request.setAttribute("errors", errors);
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("/WEB-INF/register/eventCreationForm.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		try {
			Evenement event = eventService.insertEvent(title, description, fileName, authorId);
			request.setAttribute("flash.message", "Bravo, evenement cree !");
			response.sendRedirect("EventLinkServlet?index=" + event.getId());
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error with EventService", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}
}
