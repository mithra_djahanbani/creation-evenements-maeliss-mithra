package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(RegisterServlet.class.getName());

	@Inject
	private EventService eventService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/register.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Map<String, String> errors = new HashMap<>();

		try {
			if (username == null || username.trim().isEmpty()) {
				errors.put("username", "A username must be specified");
			}
			if (password == null || password.trim().isEmpty()) {
				errors.put("password", "A password must be specified");
			}
			if (eventService.isUsernameAlreadySet(username)) {
				errors.put("usernameAlreadyExists", "This username already exists");
			}
			if (!errors.isEmpty()) {
				request.setAttribute("errors", errors);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/register.jsp");
				requestDispatcher.forward(request, response);
				return;
			}
			int authorId = eventService.insertAuthor(username, password).getId();

			HttpSession session = request.getSession();
			session.setAttribute("username", username);
			session.setAttribute("id", authorId);
			request.setAttribute("flash.message", "Bravo, compte crée !");
			response.sendRedirect("ListEventServlet");
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error Event Service", e);
			response.sendError(500, "Something wrong happend, please contact the support");
		}
	}
}
