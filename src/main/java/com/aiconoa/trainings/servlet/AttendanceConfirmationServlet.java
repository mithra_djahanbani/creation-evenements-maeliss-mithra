package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;
import com.aiconoa.trainings.service.Validation;

/**
 * Servlet implementation class AttendanceConfirmationServlet
 */
@WebServlet("/AttendanceConfirmationServlet")
public class AttendanceConfirmationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(AttendanceConfirmationServlet.class.getName());

	@Inject
	private EventService eventService;
	
	@Inject
	private Validation validation;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String token = request.getParameter("token");
			if (!eventService.isTokenValid(token)) {
				response.sendError(404);
				return;
			}
			int eventId = eventService.findEventIdByToken(token);
			request.setAttribute("index", eventId);
			request.setAttribute("token", token);
			RequestDispatcher requestDispatcher = request
					.getRequestDispatcher("/WEB-INF/register/attendanceConfirmationForm.jsp");
			requestDispatcher.forward(request, response);
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error with EventService", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String index = request.getParameter("index");
		if (!validation.isParameterNumerical(index)) {
			response.sendError(404);
			return;
		}

		boolean isConfirmed = false;
		String submit = request.getParameter("confirm");
		if (!submit.matches("^(?:True|False)$")) {
			response.sendError(404);
			return;
		}
		if (submit.matches("True")) {
			isConfirmed = Boolean.parseBoolean(submit);
		}
		if (submit.matches("False")) {
			isConfirmed = Boolean.parseBoolean(submit);
		}

		try {
			String token = request.getParameter("token");
			if (!eventService.isTokenValid(token)) {
				response.sendError(404);
				return;
			}
			if (isConfirmed) {
				eventService.updateStatus("confirmed", token);
			} else {
				eventService.updateStatus("declined", token);
			}
			response.sendRedirect("EventLinkServlet?index=" + index);
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error with EventService", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}
}
