package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;
import com.aiconoa.trainings.service.Validation;

/**
 * Servlet implementation class ListEventServlet
 */
@WebServlet("/ListEventServlet")
public class ListEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(ListEventServlet.class.getName());

	@Inject
	private EventService eventService;

	@Inject
	private Validation validation;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		if (isSession) {
			request.setAttribute("isConnected", isSession);
		}

		List<Evenement> events;
		try {
			events = eventService.selectEvents();
			request.setAttribute("events", events);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/listEvent.jsp");
			requestDispatcher.forward(request, response);
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error with EventService", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		if (!isSession) {
			response.sendRedirect("Login");
		} else {
			response.sendRedirect("EventCreationServlet");
		}
	}
}
