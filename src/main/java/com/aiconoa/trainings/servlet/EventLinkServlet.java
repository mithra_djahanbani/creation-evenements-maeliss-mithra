package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.entity.Invitation;
import com.aiconoa.trainings.entity.Participation;
import com.aiconoa.trainings.service.EventService;
import com.aiconoa.trainings.service.EventServiceException;
import com.aiconoa.trainings.service.Validation;

/**
 * Servlet implementation class EventLinkServlet
 */
@WebServlet("/EventLinkServlet")
public class EventLinkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(EventLinkServlet.class.getName());

	@Inject
	private EventService eventService;

	@Inject
	private Validation validation;

	@Inject
	Event<Participation> myEvent;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		int authorId = 0;
		if (!isSession) {
			response.sendRedirect("Login");
			return;
		}
		request.setAttribute("isConnected", isSession);
		
		String authorIndex = session.getAttribute("id").toString();
		if (!validation.isParameterNumerical(authorIndex)) {
			response.sendError(404);
			return;
		}
		authorId = Integer.parseInt(authorIndex);

		String ind = request.getParameter("index");
		if (!validation.isParameterNumerical(ind)) {
			response.sendError(404);
			return;
		}

		try {
			int eventId = Integer.parseInt(ind);

			boolean isEventFromAuthor = eventService.isEventFromAuthor(eventId, authorId);
			if (!isEventFromAuthor) {
				response.sendError(404, "Access denied");
				return;
			}

			Evenement event;
			event = eventService.selectEvent(eventId);
			request.setAttribute("event", event);

			List<List<Invitation>> invitations = eventService.updateInvitationsStateOfEvent(eventId);
			List<Invitation> pendingInvitations = invitations.get(0);
			List<Invitation> confirmedInvitations = invitations.get(1);
			List<Invitation> declinedInvitations = invitations.get(2);

			request.setAttribute("pendingInvitations", pendingInvitations);
			request.setAttribute("confirmedInvitations", confirmedInvitations);
			request.setAttribute("declinedInvitations", declinedInvitations);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/eventLink.jsp");
			requestDispatcher.forward(request, response);

		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error Event Service", e);
			response.sendError(500, "Something wrong happend, please contact the support");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		boolean isSession = validation.hasSessionUsername(session);
		if (!isSession) {
			response.sendRedirect("Login");
			return;
		}

		String ind = request.getParameter("index");
		if (!validation.isParameterNumerical(ind)) {
			response.sendError(404);
			return;
		}
		int eventId = Integer.parseInt(ind);

		String adressesParam = request.getParameter("textarea");
		try {
			Map<String, String> errors = validation.isEmailValid(adressesParam);
			if (!errors.isEmpty()) {
				request.setAttribute("errors", errors);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/register/eventLink.jsp");
				requestDispatcher.forward(request, response);
				return;
			}

			String[] adresses = adressesParam.split(";");
			for (String email : adresses) {
				if (!eventService.isEmailAlreadySet(email)) {
					int userId = eventService.insertUser(email).getId();
					String hash = eventService.generateHash(email, eventId);
					myEvent.fire(new Participation(hash));
					eventService.insertInvitation(email, eventId, userId, hash);
				}
			}
			response.sendRedirect("EventLinkServlet?index=" + eventId);
		} catch (EventServiceException e) {
			LOGGER.log(Level.SEVERE, "Error Event Service", e);
			response.sendError(500, "Something wrong happend, please contact the support");
			return;
		}
	}
}
