package com.aiconoa.trainings.ejb;

import java.util.Random;

import javax.ejb.Stateless;

@Stateless
public class RandomNumberEJB {
	
	public int generateRandomInteger(int max){
		return new Random().nextInt(max);
	}

}
