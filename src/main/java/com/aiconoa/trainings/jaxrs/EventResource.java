package com.aiconoa.trainings.jaxrs;

import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.aiconoa.trainings.entity.Author;
import com.aiconoa.trainings.entity.Evenement;
import com.aiconoa.trainings.entity.Invitation;
import com.aiconoa.trainings.entity.Participation;
import com.aiconoa.trainings.entity.User;
import com.aiconoa.trainings.service.EventService;

@Secured
@Path("/event")
public class EventResource {

	@Inject
	private EventService eventService;
	
	@Inject
	Event<Participation> myEvent;
	
	
	@GET
	@Path("/user/{userid}/event/{eventid}")
	public Invitation findInvitationByUserAndEvent(@PathParam("userid") int userId, @PathParam("eventid") int eventId) {
		return eventService.findInvitationByUserAndEvent(userId, eventId);
	}
	
	
	@GET
	@Path("/user/{id}")
	public List<Invitation> getAllInvitationsByUser(@PathParam("id") int userId) {
		return eventService.findInvitationsByUser(userId);
	}
	
	@GET
	
	@Path("/events")
	public List<Evenement> getAllEvents() {
		return eventService.selectEvents();
	}

	
	@POST
	@Path("/events")
	public void postEvent(Evenement event) {
		eventService.insertEvent(event.getTitle(),event.getDescription(), "", 15);
	}
	
	
	@GET
	@Path("/events/{id}")
	public List<Invitation> getEventInfo(@PathParam("id") int id) {
		return eventService.selectInvitationsByEventId(id);
	}
	
	
	@GET
	@Path("/events/{id}/pending")
	public List<Invitation> getPending(@PathParam("id") int id) {
		return eventService.updateInvitationsStateOfEvent(id).get(0);
	}

	@GET
	@Path("/events/{id}/confirmed")
	public List<Invitation> getConfirmed(@PathParam("id") int id) {
		return eventService.updateInvitationsStateOfEvent(id).get(1);
	}
	
	
	@GET
	@Path("/events/{id}/declined")
	public List<Invitation> getDeclined(@PathParam("id") int id) {
		return eventService.updateInvitationsStateOfEvent(id).get(2);
	}
	
	
	@POST
	@Path("/events/{id}")
	public void insertUser(User user2, @PathParam("id") int eventId) {
		User user = eventService.insertUser(user2.getEmail()); 
		String hash = eventService.generateHash(user.getEmail(), eventId);
		myEvent.fire(new Participation(hash));
		eventService.insertInvitation(user.getEmail(), eventId, user.getId(), hash);
	}

	
	@POST
	@Path("/author-registration")
	public void insertAuthor(Author author) {
		eventService.insertAuthor(author.getUsername(), author.getPassword()); 
	}
	
	
	@PUT
	@Path("/attendence-confirmation/{token}")
	public void updateParticipation(@PathParam("token") String token, String status) {
		eventService.updateStatus(status, token);
	}
	
}
