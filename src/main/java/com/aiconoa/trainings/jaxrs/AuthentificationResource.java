package com.aiconoa.trainings.jaxrs;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.aiconoa.trainings.service.EventService;

@Path("/authentification")
public class AuthentificationResource {

	@Inject
	private EventService eventService;
	
	@POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response authenticateUser(Credentials credentials) {

        try {
        	
        	String username = credentials.getUsername();
            String password = credentials.getPassword();

            // Authenticate the user using the credentials provided
            eventService.isAuthorValid(username, password);

            // Issue a token for the user
            String token = eventService.generateUsernameToken(username);
            //update token in DB
            eventService.provideAuthorSessionId(username, token);

            // Return the token on the response
            return Response.ok(token).build();

        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }      
    }
    
}
