package com.aiconoa.trainings.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/helloworld")
public class HelloWorldResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getHelloWorldJSON(){
		return "[\"hello world json!\"]";
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getHelloWorldHTML(){
		return "hello world resource!";
	}

}
