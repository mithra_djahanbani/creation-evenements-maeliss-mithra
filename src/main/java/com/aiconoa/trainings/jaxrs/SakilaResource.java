package com.aiconoa.trainings.jaxrs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import com.aiconoa.trainings.ejb.SakilaService;
import com.aiconoa.trainings.entity.Film;

@Path("/sakila")
public class SakilaResource {
	
	private static final int NUMBER_OF_FILM_PER_PAGE = 10;
	
	@Inject
	private SakilaService sakilaService;
	
	@GET
	@Path("/films/{id}")
	public Film getFilmById(@PathParam("id") Long id){
		return sakilaService.findById(id);
	}
	
	/*@GET
	@Path("/films/1")
	public Film getFilmById(){
		return sakilaService.findById(1L);
	}*/
	
	@GET
	@Path("/films")
	public List<Film> getAllFilms(@QueryParam("page") Integer page){
		int offset= 0;
		if (page != null && page > 1) {
			offset = (page - 1) * NUMBER_OF_FILM_PER_PAGE;
		}
		return sakilaService.findAll(offset, NUMBER_OF_FILM_PER_PAGE);
	}
	

	/*@POST
	@Path("/films")
	public void postFilm(@FormParam("title") String title, @FormParam("language_id") Long languageId ){
		
		Film film = new Film();
		film.setTitle(title);
		film.setLanguageId(languageId);
		sakilaService.create(film);
	}*/
	
	@POST
	@Path("/films")
	public void postFilm(Film film){
		sakilaService.create(film);
	}
	
	@PUT
	@Path("/films/{id}")
	public void putFilm(Film film, @PathParam("id") Long id){
		film.setId(id);
		sakilaService.updateFilm(film);
	}
	
	@DELETE
	@Path("/films/{id}")
	public void deleteFilm(@PathParam("id") Long id){
		sakilaService.delete(id);
	}
}
