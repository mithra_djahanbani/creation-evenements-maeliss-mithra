package com.aiconoa.trainings.apiclient;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import com.aiconoa.trainings.entity.Evenement;


public class RestApiClient {
	
	public static void main(String[] args) {

		Client client = ClientBuilder.newClient();
		
		List<Evenement> events = client.target("http://localhost:8080/creation-evenements-maeliss-mithra/api/event/events")
		        .request(MediaType.APPLICATION_JSON)
		        .get(new GenericType<List<Evenement>>(){});
		System.out.println("List of events");
		for (Evenement evenement : events) {
			System.out.println(evenement);
		}
		
		
		/*List<Invitation> invitationsByUser = client.target("http://localhost:8080/creation-evenements-maeliss-mithra/api/event/user/18")
		        .request(MediaType.APPLICATION_JSON)
		        .get(new GenericType<List<Invitation>>(){});
		System.out.println("List of invitations");
		for (Invitation invitation : invitationsByUser) {
			System.out.println(invitation.toString());
		}
		
		System.out.println("Enter the number of the event");
		Scanner sc = new Scanner(System.in);
		int eventId = sc.nextInt();
		Invitation invitationByUserByEvent = client.target("http://localhost:8080/creation-evenements-maeliss-mithra/api/event/user/18/event/"+eventId)
		        .request(MediaType.APPLICATION_JSON)
		        .get(new GenericType<Invitation>(){});
		
		System.out.println("Change your status");
		Scanner sc2 = new Scanner(System.in);
		String status = sc2.nextLine();
		JsonObject jsonObject = Json.createObjectBuilder().add("status", status).build();
		
		client.target("http://localhost:8080/creation-evenements-maeliss-mithra/api/event/attendence-confirmation/"+invitationByUserByEvent.getToken())
			  .request(MediaType.APPLICATION_JSON)
			  .put(Entity.json(status));
		    
		System.out.println(jsonObject);
		System.out.println(invitationByUserByEvent.toString());*/
		
	}

}
