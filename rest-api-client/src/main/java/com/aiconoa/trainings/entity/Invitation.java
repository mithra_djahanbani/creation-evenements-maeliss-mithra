package com.aiconoa.trainings.entity;


public class Invitation {

	private String email;
	private String date;
	private String token;
	private String status;
	
	
	private Evenement event;
	
	
	private User user;
	
	public Invitation() {
		super();
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Evenement getEvent() {
		return event;
	}

	public void setEvent(Evenement event) {
		this.event = event;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Invitation [user=" + user.toString() + ", event=" + event.toString() + ", status="
				+ status +"]";
	}
	
}
