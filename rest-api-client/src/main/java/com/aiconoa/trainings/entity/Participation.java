package com.aiconoa.trainings.entity;

public class Participation {
	
	private String hash;

	public Participation(String hash) {
		super();
		this.hash = hash;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
